import 'package:english_words/english_words.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:getwidget/getwidget.dart';
import 'package:provider/provider.dart';
import 'package:wizard_score_companion/my_home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => MyAppState(),
      child: MaterialApp(
        title: 'Wizard Score Companion',
        theme: ThemeData(
          useMaterial3: true,
          colorScheme: ColorScheme.fromSeed(seedColor: Color(0xFFab44ec)),
        ),
        home: MyHomePage(),
      ),
    );
  }
}

class MyAppState extends ChangeNotifier {
  Widget currentWidget = InitialPage();

  void setCurrentWidget(Widget widget) {
    currentWidget = widget;
    notifyListeners();
  }

  bool randomizeValue = true;

  void setRandomizeValue(bool value) {
    randomizeValue = value;
    notifyListeners();
  }
}

class NewGamePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    final theme = Theme.of(context);
    final styleW300 = theme.textTheme.bodyMedium!.copyWith(
      color: theme.colorScheme.primary,
      fontWeight: FontWeight.w300,
      fontSize: 25.0,
    );
    final styleW600 = theme.textTheme.displayMedium!.copyWith(
      color: Colors.white,
      fontWeight: FontWeight.w600,
      // fontSize: 25.0,
    );
    final styleW600Grey = theme.textTheme.bodyMedium!.copyWith(
      color: Color(0xFF878789),
      fontWeight: FontWeight.w300,
      fontSize: 18.0,
    );

    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                Text(
                  "Edit",
                  style: styleW300,
                ),
                Expanded(child: Text("")),
                CloseButton(widget: InitialPage()),
              ],
            ),
            SizedBox(height: 15.0),
            Text(
              "New Game",
              style: styleW600,
            ),
            SizedBox(height: 15.0),
            Text(
              "PLAYERS",
              style: styleW600Grey,
            ),
            SizedBox(height: 15.0),
            RoundedContainer(widgets: [
              NameInputField(),
              NameInputField(),
              NameInputField(),
              NameInputField(),
              NameInputField(),
              NameInputField(),
            ]),
            SizedBox(height: 15.0),
            Text(
              "A game requires 2-6 players.",
              style: styleW600Grey,
            ),
            SizedBox(height: 15.0),
            ElevatedButton.icon(
                onPressed: () {
                  appState.setCurrentWidget(ConfigureGamePage());
                },
                icon: Icon(Icons.edit,
                    color: theme.colorScheme.primary, size: 30.0),
                style: ButtonStyle(
                    backgroundColor: MaterialStateColor.resolveWith(
                        (states) => Color(0xFF4e4d50)),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                            side: BorderSide(color: Color(0xFF4e4d50))))),
                label: Padding(
                  padding:
                      const EdgeInsets.only(left: 8.0, bottom: 8.0, top: 8.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Configure Game",
                        textScaleFactor: 2,
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w300),
                      ),
                      Expanded(child: Text("")),
                      Icon(
                        Icons.keyboard_arrow_right_rounded,
                        color: Color(0xFF989894),
                        size: 40.0,
                      ),
                    ],
                  ),
                )),
            SizedBox(height: 15.0),
            Text(
              "This is the order in which you will play the game.\nPlease make sure that everyone has taken their respective seat.\nThe first dealer can be selected on the next page.",
              style: styleW600Grey,
            ),
          ],
        ),
      ),
    );
  }
}

class GamePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    final theme = Theme.of(context);
    final styleWhite = theme.textTheme.displayMedium!.copyWith(
      color: Colors.white,
      fontWeight: FontWeight.w400,
    );
    final stylePurple = theme.textTheme.displayMedium!.copyWith(
      color: theme.colorScheme.primary,
      fontWeight: FontWeight.w400,
    );
    final styleGrey = theme.textTheme.bodyLarge!.copyWith(
      color: Colors.grey,
      fontWeight: FontWeight.w400,
    );
    final styleGreen = theme.textTheme.bodyLarge!.copyWith(
      color: Colors.green,
      fontWeight: FontWeight.w400,
    );
    final styleRed = theme.textTheme.bodyLarge!.copyWith(
      color: Colors.red,
      fontWeight: FontWeight.w400,
    );

    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                SubTextWidget(
                  alignment: CrossAxisAlignment.start,
                  mainWidget: Text(
                    "4 / 15",
                    style: styleWhite,
                  ),
                  secondaryWidgets: Text(
                    "round / total",
                    style: styleGrey,
                  ),
                ),
                Expanded(child: Text("")),
                Image.asset(
                  'assets/images/MainLogo.jpg',
                  height: 60.0,
                ),
                Expanded(child: Text("")),
                SubTextWidget(
                    alignment: CrossAxisAlignment.end,
                    mainWidget: Text(
                      "-1",
                      style: styleWhite,
                    ),
                    secondaryWidgets: Text(
                      "difference",
                      style: styleGrey,
                    )),
              ],
            ),
            Row(children: [
              Expanded(
                child: ElevatedButton(
                  onPressed: () {},
                  style: ButtonStyle(
                      backgroundColor: MaterialStateColor.resolveWith(
                          (states) => Color(0xFF8E8E93))),
                  child: Text(""),
                ),
              ),
            ]),
            SizedBox(height: 15.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                SubTextWidget(
                  alignment: CrossAxisAlignment.start,
                  mainWidget: Text(
                    "Emma",
                    style: stylePurple,
                  ),
                  secondaryWidgets: Text(
                    "50",
                    style: styleGrey,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, bottom: 12.0),
                  child: RotatedBox(
                    quarterTurns: -2,
                    child: Icon(
                      Icons.style,
                      color: theme.colorScheme.primary,
                    ),
                  ),
                ),
                Expanded(child: Text("")),
                SubTextWidget(
                    alignment: CrossAxisAlignment.end,
                    mainWidget: Text(
                      "- / 3",
                      style: styleWhite,
                    ),
                    secondaryWidgets: Text(
                      "actual / bids",
                      style: styleGrey,
                    )),
              ],
            ),
            SizedBox(height: 15.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                SubTextWidget(
                  alignment: CrossAxisAlignment.start,
                  mainWidget: Text(
                    "James",
                    style: styleWhite,
                  ),
                  secondaryWidgets: Text(
                    "50",
                    style: styleGrey,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, bottom: 12.0),
                  child: Icon(
                    Icons.looks_one_rounded,
                    color: theme.colorScheme.primary,
                  ),
                ),
                Expanded(child: Text("")),
                SubTextWidget(
                    alignment: CrossAxisAlignment.end,
                    mainWidget: TextButton(
                      onPressed: () => showDialog(
                        context: context,
                        builder: (BuildContext context) => Dialog(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              color: Colors.black,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text('Players Name'),
                                  const SizedBox(height: 15),
                                  FloatingActionButton(
                                    onPressed: () {},
                                    shape: CircleBorder(),
                                    child: Text("0"),
                                  ),
                                  FloatingActionButton(
                                    onPressed: () {},
                                    shape: CircleBorder(),
                                    child: Text("1"),
                                  ),
                                  FloatingActionButton(
                                    onPressed: () {},
                                    shape: CircleBorder(),
                                    child: Text("2"),
                                  ),
                                  FloatingActionButton(
                                    onPressed: () {},
                                    shape: CircleBorder(),
                                    child: Text("3"),
                                  ),
                                  FloatingActionButton(
                                    onPressed: () {},
                                    shape: CircleBorder(),
                                    child: Text("4"),
                                  ),
                                  FloatingActionButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    shape: CircleBorder(),
                                    child: Icon(
                                      Icons.done,
                                      color: theme.colorScheme.primary,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      child: Text(
                        "- / 3",
                        style: styleWhite,
                      ),
                    ),
                    secondaryWidgets: Text(
                      "actual / bids",
                      style: styleGrey,
                    )),
              ],
            ),
            Expanded(child: Text("")),
            Row(
              children: [
                SizedBox(width: 30.0),
                TextButton.icon(
                    onPressed: () {
                      appState.setCurrentWidget(InitialPage());
                    },
                    style: ButtonStyle(
                      minimumSize: MaterialStatePropertyAll(Size(90.0, 90.0)),
                      foregroundColor: MaterialStateColor.resolveWith(
                          (states) => Color(0xFF878789)),
                    ),
                    icon: Icon(
                      Icons.home_sharp,
                    ),
                    label: Text("Main Menu")),
                Expanded(child: Text("")),
                TextButton.icon(
                    onPressed: () {},
                    style: ButtonStyle(
                      minimumSize: MaterialStatePropertyAll(Size(90.0, 90.0)),
                      foregroundColor: MaterialStateColor.resolveWith(
                          (states) => Color(0xFF878789)),
                    ),
                    icon: Icon(Icons.more_horiz),
                    label: Text("More Actions")),
                SizedBox(width: 30.0),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class SubTextWidget extends StatelessWidget {
  const SubTextWidget({
    super.key,
    required this.mainWidget,
    required this.secondaryWidgets,
    required this.alignment,
  });

  final Widget mainWidget;
  final Widget secondaryWidgets;
  final CrossAxisAlignment alignment;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: alignment,
      children: [
        mainWidget,
        secondaryWidgets,
      ],
    );
  }
}

class StatisticsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final styleW600 = theme.textTheme.displayMedium!.copyWith(
      color: Colors.white,
      fontWeight: FontWeight.w600,
      // fontSize: 25.0,
    );
    final styleW600Grey = theme.textTheme.bodyMedium!.copyWith(
      color: Color(0xFF878789),
      fontWeight: FontWeight.w300,
      fontSize: 18.0,
    );

    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              mainAxisSize: MainAxisSize.max,
              children: [
                CloseButton(widget: InitialPage()),
              ],
            ),
            SizedBox(height: 15.0),
            Text(
              "Statistics",
              style: styleW600,
            ),
            SizedBox(height: 15.0),
            Text(
              "OVERVIEW",
              style: styleW600Grey,
            ),
            SizedBox(height: 15.0),
            RoundedContainer(widgets: [
              InfoDataField(title: "Best result", info: "630"),
              InfoDataField(title: "Worst result", info: "-50"),
              InfoDataField(title: "Games played", info: "34"),
              InfoDataField(title: "Time spent", info: "2 days, 12 hours"),
            ]),
            SizedBox(height: 15.0),
            Text(
              "RANKINGS",
              style: styleW600Grey,
            ),
            SizedBox(height: 15.0),
            RoundedContainer(
              widgets: [
                ButtonDataField(
                  title: "Most successful",
                  info: "Emma",
                  linkedWidget: Placeholder(),
                ),
                ButtonDataField(
                  title: "Most games played",
                  info: "Alex",
                  linkedWidget: Placeholder(),
                ),
                ButtonDataField(
                  title: "Most points in a game",
                  info: "Alex",
                  linkedWidget: Placeholder(),
                ),
              ],
            ),
            SizedBox(height: 30.0),
            RoundedContainer(
              widgets: [
                ButtonDataField(
                  title: "Players",
                  info: "4",
                  linkedWidget: Placeholder(),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class InfoDataField extends StatelessWidget {
  const InfoDataField({
    super.key,
    required this.title,
    required this.info,
  });

  final bool enabled = false;
  final String title;
  final String info;

  @override
  Widget build(BuildContext context) {
    return GenericDataField(
      enabled: enabled,
      title: title,
      valueWidget: Text(
        info,
        style: TextStyle(
            color: Color(0xFF878789),
            fontWeight: FontWeight.w400,
            fontSize: 20.0),
      ),
    );
  }
}

class ButtonDataField extends StatelessWidget {
  const ButtonDataField({
    super.key,
    required this.title,
    required this.info,
    required this.linkedWidget,
  });

  final bool enabled = true;
  final String title;
  final String info;
  final Widget linkedWidget;

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    return GenericDataField(
      enabled: enabled,
      title: title,
      onPressed: () {
        if (linkedWidget is Placeholder) {
          final snackBar = SnackBar(
            content: const Text('Not implemented yet.'),
            action: SnackBarAction(
              label: 'Back to start',
              onPressed: () {
                appState.setCurrentWidget(InitialPage());
              },
            ),
          );

          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
        appState.setCurrentWidget(linkedWidget);
      },
      linkedWidget: linkedWidget,
      valueWidget: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            info,
            style: TextStyle(
                color: Color(0xFF878789),
                fontWeight: FontWeight.w400,
                fontSize: 20.0),
          ),
          Icon(
            Icons.chevron_right_sharp,
            color: Color(0xFF878789),
            size: 30.0,
          )
        ],
      ),
    );
  }
}

void emulateClickOnWidget(GlobalKey widgetGlobalKey) async {
  RenderBox renderbox =
      widgetGlobalKey.currentContext!.findRenderObject() as RenderBox;
  Offset position = renderbox.localToGlobal(Offset.zero);
  double x = position.dx;
  double y = position.dy;

  // print(x);
  // print(y);
  // print(widgetGlobalKey);
  GestureBinding.instance.handlePointerEvent(PointerDownEvent(
    position: Offset(x, y),
  ));

  await Future.delayed(Duration(milliseconds: 100));

  GestureBinding.instance.handlePointerEvent(PointerUpEvent(
    position: Offset(x, y),
  ));
}

class SwitchDataField extends StatelessWidget {
  SwitchDataField({
    super.key,
    required this.title,
  });

  final String title;
  final bool enabled = true;
  final mySwitchKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    final theme = Theme.of(context);

    return GenericDataField(
      enabled: enabled,
      title: title,
      onPressed: () {
        emulateClickOnWidget(mySwitchKey);
      },
      valueWidget: Switch(
        key: mySwitchKey,
        value: appState.randomizeValue,
        activeColor: theme.colorScheme.primary,
        onChanged: (bool value) {
          appState.setRandomizeValue(value);
        },
      ),
    );
  }
}

enum BidsConfig { notRestricted, mustNotMatchCardAmount, mustMatchCardAmount }

extension BidsConfigExtension on BidsConfig {
  String get value {
    switch (this) {
      case BidsConfig.notRestricted:
        return "are not restricted";
      case BidsConfig.mustNotMatchCardAmount:
        return "must NOT match card amount";
      case BidsConfig.mustMatchCardAmount:
        return "must match card amount";
      default:
        return "";
    }
  }
}

enum Variety { standard, anniversary }

extension VarietyExtension on Variety {
  String get value {
    switch (this) {
      case Variety.standard:
        return "Standard / Fantasy";
      case Variety.anniversary:
        return "Anniversary (Bomb, Cloud)";
      default:
        return "";
    }
  }
}

class RadioDataFieldsBidsConfig extends StatefulWidget {
  const RadioDataFieldsBidsConfig({
    super.key,
  });

  @override
  State<RadioDataFieldsBidsConfig> createState() =>
      _RadioDataFieldsBidsConfigState();
}

class _RadioDataFieldsBidsConfigState<T extends Enum>
    extends State<RadioDataFieldsBidsConfig> {
  final bool enabled = true;
  final List<GlobalKey> myRadiosKeys =
      List.generate(BidsConfig.values.length, (index) => GlobalKey());
  final List<Function()> myOnPressedFunctions =
      List.generate(BidsConfig.values.length, (index) => () {});

  BidsConfig _bidsConfiguration = BidsConfig.mustNotMatchCardAmount;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    BidsConfig bidsConfigOption;

    for (int i = 0; i < BidsConfig.values.length; i++) {
      myOnPressedFunctions[i] = () {
        emulateClickOnWidget(myRadiosKeys[i]);
      };
    }

    int i = 0;
    return RoundedContainer(widgets: [
      for (bidsConfigOption in BidsConfig.values) ...[
        GenericDataField(
          enabled: enabled,
          title: bidsConfigOption.value,
          onPressed: myOnPressedFunctions[i++],
          valueWidget: GFRadio<BidsConfig>(
            key: myRadiosKeys[bidsConfigOption.index],
            type: GFRadioType.custom,
            activeIcon: Icon(
              Icons.check,
              color: theme.colorScheme.primary,
              size: 35.0,
            ),
            size: GFSize.MEDIUM,
            value: bidsConfigOption,
            groupValue: _bidsConfiguration,
            onChanged: (value) {
              setState(() {
                _bidsConfiguration = value;
              });
            },
            inactiveIcon: Icon(null),
            customBgColor: Colors.transparent,
            activeBgColor: Colors.transparent,
            activeBorderColor: Colors.transparent,
            inactiveBgColor: Colors.transparent,
            inactiveBorderColor: Colors.transparent,
          ),
        ),
      ],
    ]);
  }
}

class RadioDataFieldsVariety extends StatefulWidget {
  const RadioDataFieldsVariety({
    super.key,
  });

  @override
  State<RadioDataFieldsVariety> createState() => _RadioDataFieldsVarietyState();
}

class _RadioDataFieldsVarietyState<T extends Enum>
    extends State<RadioDataFieldsVariety> {
  final bool enabled = true;
  final List<GlobalKey> myRadiosKeys =
      List.generate(Variety.values.length, (index) => GlobalKey());
  final List<Function()> myOnPressedFunctions =
      List.generate(Variety.values.length, (index) => () {});

  Variety _varietyConfiguration = Variety.standard;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    Variety varietyOption;

    for (int i = 0; i < Variety.values.length; i++) {
      myOnPressedFunctions[i] = () {
        emulateClickOnWidget(myRadiosKeys[i]);
      };
    }

    int i = 0;
    return RoundedContainer(widgets: [
      for (varietyOption in Variety.values) ...[
        GenericDataField(
          enabled: enabled,
          title: varietyOption.value,
          onPressed: myOnPressedFunctions[i++],
          valueWidget: GFRadio<Variety>(
            key: myRadiosKeys[varietyOption.index],
            type: GFRadioType.custom,
            activeIcon: Icon(
              Icons.check,
              color: theme.colorScheme.primary,
              size: 35.0,
            ),
            size: GFSize.MEDIUM,
            value: varietyOption,
            groupValue: _varietyConfiguration,
            onChanged: (value) {
              setState(() {
                _varietyConfiguration = value;
              });
            },
            inactiveIcon: Icon(null),
            customBgColor: Colors.transparent,
            activeBgColor: Colors.transparent,
            activeBorderColor: Colors.transparent,
            inactiveBgColor: Colors.transparent,
            inactiveBorderColor: Colors.transparent,
          ),
        ),
      ],
    ]);
  }
}

class FirstDealerDataField extends StatefulWidget {
  const FirstDealerDataField({
    super.key,
    required this.title,
    required this.players,
  });

  final String title;
  final List<String> players;

  @override
  State<FirstDealerDataField> createState() => _FirstDealerDataFieldState();
}

class _FirstDealerDataFieldState extends State<FirstDealerDataField> {
  final bool enabled = true;
  final myDropdownButtonKey = GlobalKey();

  bool switchValue = true;
  String firstDealerValue = "";

  @override
  Widget build(BuildContext context) {
    final appState = context.watch<MyAppState>();
    final theme = Theme.of(context);

    Widget valueWidget;
    if (appState.randomizeValue) {
      valueWidget = Text(
        "Random",
        style: TextStyle(
            color: Color(0xFF878789),
            fontWeight: FontWeight.w400,
            fontSize: 20.0),
      );
    } else {
      valueWidget = DropdownButton<String>(
        key: myDropdownButtonKey,
        dropdownColor: Color(0xFF4e4d50),
        borderRadius: BorderRadius.circular(15.0),
        value: firstDealerValue,
        style: TextStyle(
          color: theme.colorScheme.primary,
          fontWeight: FontWeight.bold,
          fontSize: 20.0,
        ),
        alignment: Alignment.center,
        icon: RotatedBox(
          quarterTurns: -2,
          child: Icon(
            Icons.style,
            color: theme.colorScheme.primary,
          ),
        ),
        items: widget.players.map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Center(child: Text(value)),
          );
        }).toList(),
        onChanged: (String? value) {
          setState(() {
            firstDealerValue = value!;
          });
        },
      );
    }

    return GenericDataField(
      enabled: enabled,
      title: widget.title,
      onPressed: () {
        emulateClickOnWidget(myDropdownButtonKey);
      },
      valueWidget: valueWidget,
    );
  }
}

class GenericDataField extends StatelessWidget {
  const GenericDataField({
    super.key,
    required this.enabled,
    required this.title,
    required this.valueWidget,
    this.linkedWidget,
    this.onPressed,
  });

  final bool enabled;
  final String title;
  final Widget valueWidget;
  final Widget? linkedWidget;
  final Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: enabled ? onPressed : null,
      style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
          minimumSize: Size(50, 30),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          alignment: Alignment.centerLeft),
      child: Container(
        color: Color(0xFF4e4d50),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Text(
                title,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w400,
                    fontSize: 20.0),
              ),
              Expanded(child: Text("")),
              valueWidget
            ],
          ),
        ),
      ),
    );
  }
}

class RoundedContainer extends StatelessWidget {
  const RoundedContainer({
    super.key,
    required this.widgets,
  });

  final List<Widget> widgets;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(15.0),
      child: Column(children: widgets),
    );
  }
}

class ConfigureGamePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    final theme = Theme.of(context);
    final styleW300 = theme.textTheme.bodyMedium!.copyWith(
      color: theme.colorScheme.primary,
      fontWeight: FontWeight.w300,
      fontSize: 25.0,
    );
    final styleW600 = theme.textTheme.displayMedium!.copyWith(
      color: Colors.white,
      fontWeight: FontWeight.w600,
      // fontSize: 25.0,
    );
    final styleW600Grey = theme.textTheme.bodyMedium!.copyWith(
      color: Color(0xFF878789),
      fontWeight: FontWeight.w300,
      fontSize: 18.0,
    );

    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              // crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                TextButton.icon(
                  style: TextButton.styleFrom(
                    minimumSize: Size.zero,
                    padding: EdgeInsets.zero,
                  ),
                  onPressed: () {
                    appState.setCurrentWidget(NewGamePage());
                  },
                  icon: Icon(
                    Icons.keyboard_arrow_left_rounded,
                    color: theme.colorScheme.primary,
                    size: 30.0,
                  ),
                  label: Text(
                    "New Game",
                    style: styleW300,
                  ),
                ),
              ],
            ),
            SizedBox(height: 15.0),
            Text(
              "Configure Game",
              style: styleW600,
            ),
            SizedBox(height: 15.0),
            Text(
              "FIRST DEALER",
              style: styleW600Grey,
            ),
            SizedBox(height: 15.0),
            RoundedContainer(widgets: [
              SwitchDataField(title: "Randomize"),
              FirstDealerDataField(title: "First dealer", players: <String>[
                '',
                'One',
                'Two',
                'Three',
                'Four',
                'ewvnjlr fvew'
              ]),
            ]),
            SizedBox(height: 15.0),
            Text(
              "BIDS ...",
              style: styleW600Grey,
            ),
            SizedBox(height: 15.0),
            RadioDataFieldsBidsConfig(),
            SizedBox(height: 15.0),
            Text(
              "VARIETY",
              style: styleW600Grey,
            ),
            SizedBox(height: 15.0),
            RadioDataFieldsVariety(),
            StyleableButton(text: "Start Game", widget: GamePage())
          ],
        ),
      ),
    );
  }
}

class NameInputField extends StatelessWidget {
  NameInputField({
    super.key,
  });
  final fieldTextController = TextEditingController();

  void clearText() {
    fieldTextController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: fieldTextController,
      keyboardType: TextInputType.name,
      textCapitalization: TextCapitalization.words,
      style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black),
          borderRadius: BorderRadius.circular(0.0),
        ),
        filled: true,
        fillColor: Color(0xFF4e4d50),
        hintText: "Not set",
        suffixIcon: IconButton(
          onPressed: () {
            clearText();
          },
          icon: Icon(
            Icons.cancel_sharp,
            color: Color(0xFF989894),
            size: 30.0,
          ),
        ),
      ),
    );
  }
}

class CloseButton extends StatelessWidget {
  const CloseButton({super.key, required this.widget});

  final Widget widget;

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();

    return SizedBox(
      height: 32.0,
      width: 32.0,
      child: FittedBox(
        child: FloatingActionButton(
          onPressed: () {
            appState.setCurrentWidget(widget);
          },
          backgroundColor: Colors.transparent,
          foregroundColor: Color(0xFF878789),
          shape: CircleBorder(
              side: BorderSide(
                  color: Colors.transparent,
                  width: 2.0,
                  strokeAlign: BorderSide.strokeAlignCenter)),
          child: Center(
            child: Icon(
              Icons.highlight_off,
              size: 58.0,
            ),
          ),
        ),
      ),
    );
  }
}

class InitialPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    // final listTiles = [];
    // peerHistory.map((p) => ListTile(title: Text(p.asLowerCase))).toList();

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/MainLogo.jpg',
            height: 300.0,
            fit: BoxFit.fitWidth,
          ),
          Text(
            "Wizard Score Companion",
            style: TextStyle(
                color: Colors.white,
                fontSize: theme.textTheme.displayMedium!.fontSize),
          ),
          StyleableButton(text: "New game", widget: NewGamePage()),
          StyleableButton(text: "Continue game", widget: Placeholder()),
          StyleableButton(text: "Statistics", widget: StatisticsPage()),
        ],
      ),
    );
  }
}

class StyleableButton extends StatelessWidget {
  const StyleableButton({
    super.key,
    required this.text,
    required this.widget,
  });

  final String text;
  final Widget widget;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    var appState = context.watch<MyAppState>();
    final styleW400 = theme.textTheme.displayMedium!.copyWith(
      color: theme.colorScheme.onPrimary,
      fontWeight: FontWeight.w400,
    );
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 50),
      child: Center(
        child: ConstrainedBox(
          constraints:
              const BoxConstraints(minWidth: double.infinity, minHeight: 100),
          child: ElevatedButton(
            onPressed: () {
              appState.setCurrentWidget(widget);
            },
            style: ButtonStyle(
                backgroundColor: MaterialStateColor.resolveWith(
                    (states) => theme.colorScheme.primary),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: theme.colorScheme.primary)))),
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: styleW400,
            ),
          ),
        ),
      ),
    );
  }
}

class BigCard extends StatelessWidget {
  const BigCard({
    super.key,
    required this.pair,
  });

  final WordPair pair;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final styleBold = theme.textTheme.displayMedium!.copyWith(
      color: theme.colorScheme.onPrimary,
      fontWeight: FontWeight.bold,
    );
    final styleW200 = theme.textTheme.displayMedium!.copyWith(
      color: theme.colorScheme.onPrimary,
      fontWeight: FontWeight.w200,
    );

    return Card(
      color: theme.colorScheme.primary,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              pair.first,
              style: styleW200,
              semanticsLabel: pair.first,
            ),
            Text(
              pair.second,
              style: styleBold,
              semanticsLabel: pair.second,
            ),
          ],
        ),
      ),
    );
  }
}
