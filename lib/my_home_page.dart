import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wizard_score_companion/main.dart';

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();

    return LayoutBuilder(builder: (context, constraints) {
      return Scaffold(
        body: Container(
          margin: EdgeInsets.all(2.0),
          color: Colors.black,
          child: appState.currentWidget,
        ),
      );
    });
  }
}
